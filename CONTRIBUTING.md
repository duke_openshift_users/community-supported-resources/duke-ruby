Please consult the [Duke Openshift Users Group Community Supported Resources Standards Guide](https://duke_openshift_users.pages.oit.duke.edu/community-supported-resources/guide/standards/) for more information on contributing to this resource.

Contributers should fork this project into their personal project, make
changes, test them, commit them and push the changes to your
personal fork, then create a Merge Request of the changes back
to the upstream project main branch, and request a maintainer
(see Maintainers.md) to review the merge request.
